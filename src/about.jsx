import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
export function aboutUs(){
    return(
        <div className="p-5  text-uppercase about container">
            <h1 className="text-center">about us</h1>
            <p className="maxw-200 text-center">t sit ducimus, perspiciatis accusantium neque quaerat
                 corporis iste vitae dolore cum voluptates ut error,
                  deserunt ipsa id nam laborum quia minima.</p>
                  <div className="blocks text-center m-5">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="sing-block p-5">
                                <h4>card title</h4>
                                <p className="maxw-200 text-center">t sit ducimus, perspiciatis accusantium neque quaerat
                 corporis iste vitae dolore cum voluptates ut error,
               .</p>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="sing-block p-5">
                            <h4>card title</h4>
                            <p className="maxw-200 text-center">t sit ducimus, perspiciatis accusantium neque quaerat
                 corporis iste vitae dolore cum voluptates ut error,
               .</p>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="sing-block p-5">
                            <h4>card title</h4>
                            <p className="maxw-200 text-center">t sit ducimus, perspiciatis accusantium neque quaerat
                 corporis iste vitae dolore cum voluptates ut error,</p>
                            </div>
                        </div>
                    </div>
                    <button className="btn btn-primary mt-5">click here</button>
                  </div>
        </div>
    );
}