import React from 'react';
import {myNav} from './navbar';
import {aboutUs} from './about';
import {contactUs} from './contact'
import {footer} from './footer'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {
  return (
    <div className="App">
      {myNav()}
      {aboutUs()}
      {contactUs()}
      {footer()}
    </div>
  );
}

export default App;
