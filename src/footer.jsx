import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

export function footer(){
    return (
        <div className="footer">
            <div className="row mx-0">
                <div className="col-md-12">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="foot-col">
                                <ul>
                                    <li>link</li>
                                    <li>link</li>
                                    <li>link</li>
                                    <li>link</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="foot-col">
                            <ul>
                                    <li>link</li>
                                    <li>link</li>
                                    <li>link</li>
                                    <li>link</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="foot-col">
                            <ul>
                                    <li>link</li>
                                    <li>link</li>
                                    <li>link</li>
                                    <li>link</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}