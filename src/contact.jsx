import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Form , Navbar  ,FormControl, InputGroup , Button } from 'react-bootstrap'

export function contactUs(){
    return(
        <div className="contact-us">
                        <h1 className="text-center text-uppercase">contact us</h1>
            <p className="maxw-200 text-center">t sit ducimus, perspiciatis accusantium neque quaerat
                 corporis iste vitae dolore cum voluptates ut error,
                  deserunt ipsa id nam laborum quia minima.</p>
            <div className="row">
                <div className="col-md-6">
                    
                        <input type="text" className="form-control" placeholder="enter your name"/>
                        <input type="email" className="form-control" placeholder="enter your email"/>
                        <input type="text" className="form-control" placeholder="enter your number"/>
                    
                </div>
                <div className="col-md-6">
                    
                    <textarea className="form-control" placeholder="enter your message">

                    </textarea>
                    
                </div>
                <button className="btn btn-primary m-auto p-3">click here</button>
            </div>
        </div>
    );
}